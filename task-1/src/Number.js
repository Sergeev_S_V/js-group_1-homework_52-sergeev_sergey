import React, {Component} from 'react';
import './Number.css';

const Number = function (props) {
    return (
        <div className="numbersBox">
            <div className="number">{props.number1}</div>
            <div className="number">{props.number2}</div>
            <div className="number">{props.number3}</div>
            <div className="number">{props.number4}</div>
            <div className="number">{props.number5}</div>
        </div>
    );
};

export default Number;