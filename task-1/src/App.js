import React, { Component } from 'react';
import './App.css';
import './NewNumbers.css';
import Number from "./Number";

class App extends Component {


    generateNumbers = () => {
        let array = [];
        for (let i = 5; i <= 36; i++) {
            array.push(i);
        }
        return array;
    };

    generateFiveRandomNumbers = () => {
        let copyGenerateNumbers = this.generateNumbers();
        let randomFiveNumbers = [];
        for (let i = 0; i < 5; i++) {
            let randomNumber = Math.floor(Math.random() * copyGenerateNumbers.length);
            randomFiveNumbers.push(copyGenerateNumbers[randomNumber]);
            copyGenerateNumbers.splice(randomNumber, 1);
        }
        this.setState({numbers: randomFiveNumbers.sort((a, b) => a > b)});
    };

    state = {
        numbers: []
    };

    componentDidMount = () => {
        this.generateFiveRandomNumbers();
    };

    changeNumbers = () => {
        this.generateFiveRandomNumbers();
    };


  render() {
    return (
        <div className="App">
            <button className="newNumbers" onClick={this.changeNumbers}>Change numbers</button>
            <Number number1={this.state.numbers[0]}
                    number2={this.state.numbers[1]}
                    number3={this.state.numbers[2]}
                    number4={this.state.numbers[3]}
                    number5={this.state.numbers[4]}
            />
        </div>
    );
  }
}

export default App;
